const db = require("../models");
const Enums = require("../helpers/enums/enums");
var bcrypt = require("bcryptjs");


const User = db.users;
const Role = db.roles;

exports.seed = (req, res) => {
    return Promise.all([

        User.create({
            name: "Administrator", middlename: "", surname: "Administrator", mainRole: Enums.Roles.Admin,
            username: "admin", email: "admin@mail", password: bcrypt.hashSync("12345", 8)
        }),

        Role.create({ name: Enums.Roles.Admin }),
        Role.create({ name: Enums.Roles.Teacher }),
        Role.create({ name: Enums.Roles.Staff }),
        Role.create({ name: Enums.Roles.Student }),
        Role.create({ name: Enums.Roles.ProgramDirector }),
        Role.create({ name: Enums.Roles.ProgramManager }),
        Role.create({ name: Enums.Roles.User }),
    ])
        .then(([adminUser, adminRole]) => {
            return Promise.all(
                [
                    adminUser.setRoles([adminRole])
                ]
            );
        })
};