module.exports = app => {
    const permit = require('../middlewares/permission');
    const Enums = require('../helpers/enums/enums');
    const programs = require("../controllers/program.controller.js");

    var router = require("express").Router();

    // Create a new Program
    router.post("/", permit([Enums.Roles.Admin]), programs.create);


    ///GET
    // Retrieve all Programs
    router.get("/", programs.findAll);
    router.get("/related/:id?", programs.findAllWithRelations);
    router.get("/:programId?/participants", programs.findAllWithParticipants);

    //Get Programs with Related Events
    router.get("/:programId?/relatedevents", permit([Enums.Roles.ProgramManager
        , Enums.Roles.ProgramDirector], true)
        , programs.findRelatedEvents);

    // Retrieve a single Program with id
    router.get("/:programId", programs.findOne);


    // Assign Managers for Program
    router.post("/:programId/assignmanagers", permit([Enums.Roles.Admin]), programs.assignManagersToProgram);

    // Assign Participants for Program
    router.post("/:programId/assignparticipants", permit([Enums.Roles.ProgramManager, Enums.Roles.ProgramDirector], true), programs.assignParticipantsToProgram);

    router.post("/:id/sendnotifications", programs.sendNotificationsForUpcomingEvents);



    // Update a Program with id
    router.put("/:id", permit([Enums.Roles.Admin]), programs.update);

    // Delete a Program with id
    router.delete("/:id", permit([Enums.Roles.Admin]), programs.delete);

    // Create a new Program
    router.delete("/", permit([Enums.Roles.Admin]), programs.deleteAll);

    app.use('/api/programs', router);
};
