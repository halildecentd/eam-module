const permission = require('../middlewares/permission');
const Enums = require('../helpers/enums/enums');
const controller = require("../controllers/student.controller");
const userController = require("../controllers/user.controller");

module.exports = app => {

  var router = require("express").Router();

  router.get("/", controller.findAll);
  router.get("/:id/details", controller.getDetails);
  router.get("/:id/passedevents", controller.findPassedEvents);
  router.get("/:id/upcomingevents", controller.findUpcomingEvents);

  router.get("/:userId?/programs", userController.findAllWithPrograms);
  router.get("/:userId?/notifications", userController.findAllWithNotifications);
  
  router.get("/:studentId?/progress/:programId?", controller.getProgress);



  router.post("/", permission([Enums.Roles.Admin]), controller.create);

  router.put("/:studentId", permission([Enums.Roles.Admin]), controller.update);


  app.use('/api/students', router);
};
