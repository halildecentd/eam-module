const permission = require('../middlewares/permission');
const Enums = require('../helpers/enums/enums');
const teacherController = require('../controllers/teacher.controller');
const userController = require('../controllers/user.controller');

module.exports = app => {

  var router = require("express").Router();

  router.get("/", teacherController.findAll);

  router.get("/:id/details", teacherController.getDetails);

  router.get("/:userId?/managedprograms/:programId?", permission([Enums.Roles.Admin, Enums.Roles.Teacher]), teacherController.findAllWithManagedPrograms);

  router.get("/:userId?/notifications", userController.findAllWithNotifications);

  //router.get("/:studentId?/progress/:programId?", controller.getProgress);

  router.post("/", permission([Enums.Roles.Admin]), teacherController.create);


  router.put("/:teacherId", permission([Enums.Roles.Admin]), teacherController.update);

  app.use('/api/teachers', router);
};
