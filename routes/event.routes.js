module.exports = app => {
    const Enums = require("../helpers/enums/enums");
    const events = require("../controllers/event.controller.js");
    const permit = require("../middlewares/permission");



    var router = require("express").Router();

    // Create a new Program
    router.post("/", permit([Enums.Roles.Admin
        , Enums.Roles.Teacher
        , Enums.Roles.Student])
        , events.create);

    // Retrieve all Programs
    router.get("/", events.findAll);
    router.get("/related/:id?", events.findAllWithRelations);

    // Retrieve a single Program with id
    router.get("/:id", events.findOne);

    // Retrieve Upcoming Events
    router.get("/upcomingevents/:programId?", events.getUpcomingEvents);

    // Retrieve all published Programs
    router.post("/:eventId/sendnotifications", events.sendNotificationsForEvent);

    router.post("/sendnotificationsforupcomingevents/:programId?", events.sendNotificationsForUpcomingEvents);



    // Update a Program with id
    router.put("/:id", events.update);

    // Delete a Program with id
    router.delete("/:id", events.delete);

    // Create a new Program
    router.delete("/", events.deleteAll);

    app.use('/api/events', router);
};
