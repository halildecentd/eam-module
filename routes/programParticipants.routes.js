module.exports = app => {
    const ProgramParticipants = require("../controllers/programParticipant.controller");


    var router = require("express").Router();

    // Create a new ProgramParticipant
    router.post("/", ProgramParticipants.create);

    // Retrieve all ProgramParticipant
    router.get("/", ProgramParticipants.findAll);
    //router.get("/related/:id?", ProgramParticipants.findAllWithRelations);

    // Retrieve a single Program with id
    router.get("/:id", ProgramParticipants.findOne);

    // Update a Program with id
    router.put("/:id", ProgramParticipants.update);

    // Delete a Program with id
    router.delete("/:id", ProgramParticipants.delete);

    // Create a new Program
    router.delete("/", ProgramParticipants.deleteAll);

    app.use('/api/programparticipants', router);
};
