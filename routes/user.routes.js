const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");

/**
* @swagger
* /findUser:
*   get:
*     tags:
*       - Users
*     name: Find user
*     summary: Finds a user
*     security:
*       - bearerAuth: []
*     consumes:
*       - application/json
*     produces:
*       - application/json
*     parameters:
*       - in: query
*         name: username
*         schema:
*           type: string
*         required:
*           - username
*     responses:
*       200:
*         description: A single user object
*         schema:
*           $ref: '#/definitions/User'
*       401:
*         description: No auth token
*/

module.exports = app => {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  var router = require("express").Router();


  //app.get("/api/test/all", controller.allAccess);
  router.get("/", controller.findAll);  

  router.get("/:userId?/programs", controller.findAllWithPrograms);

  router.get("/:userId?/managedprograms/:programId?", controller.findAllWithManagedPrograms);

  router.get("/:userId?/notifications/:isRead?", controller.findAllWithNotifications);
  
  
  ////POST
  router.post("/", controller.create);


  app.use('/api/users', router);
};
