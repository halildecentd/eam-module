require('express-async-errors');
const winston = require('winston');
const express = require("express");
winston.add(new winston.transports.File({ filename: './logs/logfile.log' }))

console.log(process.env.NODE_ENV);
if (process.env.NODE_ENV !== 'production') require('dotenv').config()

const bodyParser = require("body-parser");
const cors = require("cors");
const { authJwt } = require("./middlewares");
const error = require("./middlewares/error");


const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const seeder = require('./seeder/seed');

const app = express();


var corsOptions = {
  origin: "http://localhost:8081"
};


app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));



var isSyncActive = (process.env.SYNCDB_AND_SEED_DATA == 'true');
if (isSyncActive) {
  
  const db = require("./models");
//db.sequelize.sync();
// // //drop the table if it already exists
  db.sequelize.sync({ force: true })
    .then(() => seeder.seed())
    .then(() => {
      console.log("Drop and re-sync db.");
    });
}

console.log(`Your port is ${process.env.PORT}`);

const swaggerDefinition = {
  info: {
    title: 'MySQL Registration Swagger API',
    version: '1.0.0',
    description: 'Endpoints to test the user registration routes',
  },
  host: 'localhost:3003',
  basePath: '/',
  securityDefinitions: {
    bearerAuth: {
      type: 'apiKey',
      name: 'Authorization',
      scheme: 'bearer',
      in: 'header',
    },
  },
};
const options = {
  swaggerDefinition,
  apis: ['./routes/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);
app.get('/swagger.json', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

console.log("Auth Route");
require('./routes/auth.routes')(app);
app.use(authJwt.verifyToken);

// routes
console.log("user Route");
require('./routes/user.routes')(app);

console.log("Student Route");
require('./routes/student.routes')(app);

console.log("Teacher Route");
require('./routes/teacher.routes')(app);

console.log("program Route");
require('./routes/program.routes')(app);

console.log("Event Route");
require('./routes/event.routes')(app);

console.log("Notification Route");
require('./routes/notification.routes')(app);

console.log("User Notification Route");
require('./routes/userNotification.routes')(app);

console.log("Program Participants Route");
require('./routes/programParticipants.routes')(app);

app.use(error);

// set port, listen for requests
const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
