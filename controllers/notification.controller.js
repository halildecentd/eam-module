const db = require("../models");
const Program = db.programs;
const User = db.users;
const Notification = db.notifications;

const Op = db.Sequelize.Op;

// Create and Save a new Notification
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title || !req.body.notificationType || !req.body.eventId) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a Notification
    const notification = {
        eventId: req.body.eventId,
        title: req.body.title,
        notificationType: req.body.notificationType,
        isActive: req.body.isActive ? req.body.isActive : true,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id
    };

    // Save Notification in the database
    Notification.create(notification)
        .then(notification => {
            res.send({ message: "Notification was registered successfully!" }, { notification });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

// Retrieve all Notifications from the database.
exports.findAll = (req, res) => {

    var whereStatement = {};
    if (req.query.name) {
        whereStatement.name = { [Op.like]: '%' + req.query.title + '%' };
    }
    if (req.query.description) {
        whereStatement.description = { description: { [Op.like]: `%${req.query.description}%` } };
    }
    if (req.query.isActive) {
        whereStatement.isActive = { isActive: { [Op.eq]: req.query.isActive } };
    }

    console.log(whereStatement);
    Notification.findAll({
        where: whereStatement,
        include: [
            {
                model: User,
                as: 'NotifiedUsers',
                through: { attributes: [] }
            }
        ]
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedUserId = req.params.id;
    var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

    var whereStatement = {};
    if (relatedUserId)
        whereStatement.id = relatedUserId;
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    Notification.findAll(({
        include: {
            model: Notification
            , where: whereStatement
            , as: 'CreatorUser'
            , attributes: ['id', 'name', 'middlename', 'surname']

        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single Notification with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Notification.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Notification with id=" + id
            });
        });
};

// Update a Notification by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.userId;
    Notification.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Notification was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Notification with id=${id}. Maybe Notification was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Notification with id=" + id
            });
        });
};

// Delete a Notification with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Notification.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Notification was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Notification with id=${id}. Maybe Notification was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Notification with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
    Notification.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Notifications were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

// find all pending Notification
exports.findAllPending = (req, res) => {
    Notification.findAll({ where: { status: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving ezpenses."
            });
        });
};
