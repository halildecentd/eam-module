const db = require('../models');
const programConroller = require('../controllers/program.controller');
const Enums = require('../helpers/enums/enums');
const Program = db.programs;
const User = db.users;
const Event = db.events;
const Notification = db.notifications;

const Op = db.Sequelize.Op;

// Create and Save a new Event
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name || !req.body.description || !req.body.type || !req.body.eventDate ||
        !req.body.venue || !req.body.duration || !req.body.programId) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a Event
    const event = {
        programId: req.body.programId,
        name: req.body.name,
        description: req.body.description,
        type: req.body.type,
        venue: req.body.venue,
        duration: req.body.duration,
        eventDate: req.body.eventDate,
        isActive: req.body.isActive ? req.body.isActive : true,
        isApproved: req.body.isActive ? req.body.isActive : true,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id
    };

    let isStudent = false;
    if (req.LoginUser.mainRole === Enums.Roles.Student) {
        event.isActive = false;
        event.isApproved = false;
        isStudent = true;
    }

    Program.findByPk(event.programId)
        .then(program => {
            if (program) {
                programConroller.isUserApprovedForProgram(req.LoginUser.id, program.id)
                    .then(isApproved => {
                        if (isApproved || isStudent) {
                            Event.create(event)
                                .then(event => {
                                    let notificationTitle = `New Event Added '${event.name}'`;
                                    let notificationType = Enums.NotificationTypes.NewEvent;
                                    if (req.LoginUser.mainRole === Enums.Roles.Student) {
                                        this.sendNotificationsForExistingEvent(req.LoginUser.id, event.id, notificationTitle, notificationType, false, true)
                                            .then(result => {
                                                res.send({ message: "Event was registered successfully!", event: event });
                                            })
                                            .catch(err => {
                                                res.status(500).send({ message: err.message });
                                            });
                                    }
                                    else {
                                        this.sendNotificationsForExistingEvent(req.LoginUser.id, event.id, notificationTitle, notificationType, true, false)
                                            .then(() => {
                                                res.send({ message: "Event was registered successfully!", event: event });
                                            })
                                            .catch(err => {
                                                res.status(500).send({ message: err.message });
                                            });
                                    }
                                })
                                .catch(err => {
                                    res.status(500).send({ message: err.message });
                                });
                        }
                        else {
                            res.status(403).send({ message: `Forbidden user for adding events to program id = ${program.id}` });
                            return;
                        }
                    });
            }
            else {
                res.status(400).send({ message: `There is not program which id is : ${event.programId}` });
                return;
            }
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

// Retrieve all Events from the database.
exports.findAll = (req, res) => {

    console.log(req.LoginUser);
    var whereStatement = {};
    if (req.query.name) {
        whereStatement.name = { name: { [Op.like]: '%' + req.query.name + '%' } };
    }
    if (req.query.username) {
        whereStatement.description = { description: { [Op.like]: `%${req.query.description}%` } };
    }
    if (req.query.email) {
        whereStatement.type = { type: { [Op.eq]: req.query.type } };
    }

    console.log(whereStatement);
    Event.findAll({
        where: whereStatement
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedProgramId = req.params.id;

    var whereStatement = {};
    if (relatedProgramId)
        whereStatement.id = { [Op.eq]: relatedProgramId };

    Event.findAll(({
        include: {
            model: Program
            , where: whereStatement
            , as: 'Program'
            , attributes: ['id', 'name']
        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single Event with an id
exports.findOne = (req, res) => {
    const id = parseInt(req.params.id);

    if (isNaN(id)) {
        res.status(500).send({
            message: `'${req.params.id}' is not valid event id.`
        });
    }
    else {
        Event.findByPk(id)
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error retrieving Event with id=" + id
                });
            });
    }
};

// Update a Event by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.LoginUser.id;

    Event.findByPk(id)
        .then(event => {
            if (event) {
                programConroller.isUserApprovedForProgram(req.LoginUser.id, event.programId)
                    .then(isApproved => {
                        if (isApproved) {

                            Event.update(req.body, {
                                where: { id: id }
                            })
                                .then(num => {
                                    if (num == 1) {
                                        res.send({
                                            message: "Event was updated successfully."
                                        });
                                    } else {
                                        res.send({
                                            message: `Cannot update Event with id=${id}. Maybe Event was not found or req.body is empty!`
                                        });
                                    }
                                })
                                .catch(err => {
                                    res.status(500).send({
                                        message: "Error updating Event with id=" + id
                                    });
                                });

                        }
                        else {
                            res.status(403).send({ message: "User must have Admin, Program Director or Program Manager rights." });
                        }
                    });
            }
            else {
                res.send({
                    message: `Cannot find Event with id=${id}.`
                });
            }
        });
};

// Delete a Event with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Event.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Event was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Event with id=${id}. Maybe Event was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Event with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
    Event.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Events were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

// Send Notifications for Event to all user enrolled to program of event.
exports.sendNotificationsForEvent = async (req, res) => {

    if (!req.body.title || !req.body.notificationType) {
        res.status(400).send({
            message: "Notification Content can not be empty!"
        });
        return;
    }

    const eventId = req.params.eventId;
    const loginUserId = req.LoginUser.id;
    var whereStatement = {};
    if (eventId)
        whereStatement.id = eventId;
    else {
        res.status(400).send({ messega: "Event id must be provided" });
    }

    Event.findByPk(eventId)
        .then(event => {
            if (event) {

                programConroller.isUserApprovedForProgram(loginUserId, event.programId)
                    .then(isApproved => {
                        if (isApproved) {
                            let notificationTitle = req.body.title;
                            let notificationType = req.body.notificationType;

                            this.sendNotificationsForExistingEvent(loginUserId, event.id, notificationTitle, notificationType, true, false)
                                .then(notification => {
                                    res.send({ message: "Notification sent to program participants succesfully!", data: notification });
                                });
                        }
                        else {
                            res.status(403).send({ message: "User must have Admin, Program Director or Program Manager rights." });
                        }
                    });
            }
            else {
                res.status(400).send({ messega: `There is no event with id : ${eventId} ` });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while sending notifications for event."
            });
        });
};

// Send Notifications for Event to all user enrolled to program of event.
exports.sendNotificationsForExistingEvent = async (userId, eventId, notificationTitle, notificationType, sendToParticipants, sendToProgramManagers) => {

    return new Promise(function (resolve, reject) {
        if (!notificationTitle || !notificationType) {
            reject(new Error('Notification Content can not be empty!'));
        }

        var whereStatement = {};
        if (eventId)
            whereStatement.id = eventId;
        else {
            reject(new Error('Event id must be provided.'));
        }
        Event.findByPk(eventId)
            .then(event => {
                if (event) {
                    Notification.create({ eventId: event.id, title: notificationTitle, notificationType: notificationType, isActive: true, createdBy: userId, updatedBy: userId })
                        .then(notification => {
                            Program.findAll(({
                                include: [
                                    {
                                        model: User,
                                        as: 'Participants',
                                        through: { attributes: [] },
                                    }
                                    , {
                                        model: User,
                                        as: 'ProgramManagers',
                                        through: { attributes: [] },
                                    }
                                ],
                                where: id = event.programId
                            }))
                                .then(data => {
                                    let notifiedUsers = [];
                                    if (sendToParticipants) {
                                        Promise.all([
                                            notification.setNotifiedUsers(data[0].dataValues.Participants)
                                        ]);
                                    }
                                    if (sendToProgramManagers) {
                                        Promise.all([
                                            notification.setNotifiedUsers(data[0].dataValues.ProgramManagers)
                                        ]);
                                    }
                                    resolve(notification);
                                })
                        });
                }
                else {
                    reject(new Error(`There is no event with id : ${eventId} `));
                }
            })
            .catch(err => {
                reject(new Error(err));
            });
    });
};

// Send Notifications for Event to all user enrolled to program of event.
exports.sendNotificationsForUpcomingEvents = async (req, res) => {

    const programId = req.params.programId;
    const userId = req.params.userId;


    this.findUpcomingEvents(programId, userId)
        .then(upcomingEvents => {
            console.log(upcomingEvents);
            for (let i = 0; i < upcomingEvents.length; i++) {
                console.log(upcomingEvents[i].name + " --> " + upcomingEvents[i].id);
                Promise.all([
                    this.sendNotificationsForExistingEvent(req.LoginUser.id
                        , upcomingEvents[i].id
                        , "Upcoming Event Notification"
                        , Enums.NotificationTypes.UpcomingEvents
                        , true
                        , true)]);
            }
            res.send({ messega: "Upcoming Events", data: upcomingEvents });
        }
        ).catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while sending notifications for event."
            });
        });
};

// Find Upcoming Events
exports.findUpcomingEvents = async (programId, studentId) => {

    var whereStatement = {};
    var whereStatementForProgram = {};
    var whereStatementForStudent = {};

    whereStatement.eventDate = { [Op.gte]: Date.now() };

    if (programId) {
        whereStatementForProgram.id = { [Op.eq]: programId };
    }
    if (studentId) {
        whereStatementForStudent.id = { [Op.eq]: studentId };
    }

    return new Promise(function (resolve, reject) {
        Event.findAll({
            where: whereStatement,
            include: [
                {
                    model: Program
                    , as: 'Program'
                    , attributes: ['id', 'name']
                    , where: whereStatementForProgram
                }
            ]
            , order: [['eventDate', 'asc']]
        }
        )
            .then(data => {
                resolve(data);
            })
            .catch(err => {
                reject(err);
            });
    });




    Event.findAll(
        {
            where: whereStatement,
            include: [
                {
                    model: Program
                    , as: 'Program'
                    , attributes: ['id', 'name']
                    // , include: [
                    //     {
                    //         model: User
                    //         , as: 'Participants'
                    //         , through: { attributes: [] }
                    //         , attributes: ['id', 'name']
                    //         , where: whereStatementForStudent
                    //     }]
                    , where: whereStatementForProgram
                }
            ]
            , order: [['eventDate', 'asc']]
        }
    )
        .then(data => {
            console.log(data);
            return data;
        })
        .catch(err => {
            throw new Error(err);
        });
};


// Retrieve all Events from the database.
exports.getUpcomingEvents = (req, res) => {

    const programId = req.params.programId;

    this.findUpcomingEvents(programId)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving upcoming events."
            });
        });
};
