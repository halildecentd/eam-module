const db = require("../models");
const ProgramManager = db.programManager;
const User = db.users;
const Program = db.programs;
const Op = db.Sequelize.Op;

// Create and Save a new ProgramManager
exports.create = (req, res) => {
    // Validate request
    if (!req.body.userId || !req.body.programId) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a ProgramManager
    const programManager = {
        userId: req.body.userId,
        programId: req.body.programId,
        isActive: req.body.isActive ? req.body.isActive : true,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id
    };


    User.findByPk(programManager.userId).then(user => {
        if (user) {
            Program.findByPk(programManager.programId).then(program => {
                if (program) {
                    ProgramManager.create(programManager)
                        .then(programParticipant => {
                            res.send({ message: `User with id = ${user.id} added to program with id = ${program.id}  as manager successfully!`, data: programParticipant });
                        })
                        .catch(err => {
                            res.status(500).send({ message: err.message });
                        });
                }
                else {
                    res.status(500).send({ message: "Program is not found with id = " + programManager.programId });
                }
            });
        }
        else {
            res.status(500).send({ message: "User is not found with id = " + programManager.userId });
        }
    });
};

// Retrieve all Programs from the database.
exports.findAll = (req, res) => {

    var whereStatement = {};
    if (req.query.userId) {
        whereStatement.userId = { userId: { [Op.eq]: req.query.userId } };
    }

    console.log(whereStatement);
    ProgramManager.findAll({
        where: whereStatement
        // ,include :[
        //     {
        //         model: User
        //     }
        // ]

    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedUserId = req.params.id;
    var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

    var whereStatement = {};
    if (relatedUserId)
        whereStatement.id = relatedUserId;
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    ProgramManager.findAll(({
        include: {
            model: ProgramManager
            , where: whereStatement
            , as: 'CreatorUser'
            , attributes: ['id', 'name', 'middlename', 'surname']

        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single ProgramManager with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    ProgramManager.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving ProgramManager with id=" + id
            });
        });
};

// Update a ProgramManager by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.LoginUser.id;
    ProgramManager.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "ProgramManager was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update ProgramManager with id=${id}. Maybe ProgramManager was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating ProgramManager with id=" + id + ". Error details : " + err.message
            });
        });
};

// Delete a ProgramManager with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    ProgramManager.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "ProgramManager was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete ProgramManager with id=${id}. Maybe ProgramManager was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete ProgramManager with id=" + id
            });
        });
};

// Delete all ProgramManagers from the database.
exports.deleteAll = (req, res) => {
    ProgramManager.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Programs were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};


