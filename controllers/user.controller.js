const db = require("../models");
const config = require("../config/auth.config");
const roleEnum = require("../helpers/enums/role.enum");
const User = db.users;
const Student = db.students;
const Role = db.roles;
const Program = db.programs;
const Notification = db.notifications;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");


// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  if (!req.body.username || !req.body.email || !req.body.password || !req.body.name || !req.body.surname) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a User
  const user = {
    name: req.body.name,
    middlename: req.body.middlename,
    surname: req.body.surname,
    mainRole: req.body.mainRole,
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    isActive: req.body.isActive ? req.body.isActive : true,
    createdBy: req.LoginUser.id,
    updatedBy: req.LoginUser.id
  };

  // Save User in the database
  User.create(user)
    .then(user => {
      if (req.body.Roles) {
        Role.findAll({
          where: {
            name: {
              [Op.or]: req.body.Roles
            }
          }
        }).then(roles => {
          user.setRoles(roles).then(() => {
            res.send({ message: "User was registered successfully!" });
          });
        });
      } else {
        Role.findAll({
          where: {
            name: {
              [Op.eq]: roleEnum.Student
            }
          }
        }).then(result => {
          user.setRoles(result);
        })
          .then(() => {
            res.send({ message: "User was registered successfully!" });
          });
      }
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};

// Find a single User with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id
      });
    });
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {

  var whereStatement = {};
  if (req.query.name) {
    whereStatement.name = { [Op.like]: '%' + req.query.name + '%' };
  }
  if (req.query.username) {
    whereStatement.username = { username: { [Op.like]: `%${req.query.username}%` } };
  }
  if (req.query.email) {
    whereStatement.email = { email: { [Op.like]: `%${req.query.email}%` } };
  }

  console.log(whereStatement);
  User.findAll({
    where: whereStatement,
    include: [
      {
        model: Role,
        as: 'Roles',
        through: { attributes: [] }
      },
      {
        model: Notification,
        as: 'Notifications',
        through: { attributes: [] }
      },
      {
        model: Program,
        as: 'IncludedPrograms',
        through: { attributes: [] }
      },
      {
        model: Program,
        as: 'ManagedPrograms',
        through: { attributes: [] }
      }
    ],
    attributes: {
      exclude: ['password']
    }
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};

exports.findAllWithRelations = (req, res) => {
  const source = req.query.source;
  const relatedUserId = req.params.id;
  var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

  var whereStatement = {};
  if (relatedUserId)
    whereStatement.id = relatedUserId;
  // if (searchParams.username)
  //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


  User.findAll(({
    include: {
      model: User
      , where: whereStatement
      , as: 'CreatorUser'
      , attributes: ['id', 'name', 'middlename', 'surname']

    }
  }))
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};

//Find users with included programs
exports.findAllWithPrograms = (req, res) => {
  const userId = req.params.userId;

  var whereStatement = {};
  if (userId)
    whereStatement.id = { [Op.eq]: userId };

  User.findAll(({
    include: [
      {
        model: Program
        , as: 'IncludedPrograms'
        , through: { attributes: [] }
        , required: false
      }
    ],
    where: whereStatement
    , attributes: ['id', 'name']
  }))
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};

exports.findAllWithManagedPrograms = (req, res) => {
  const userId = req.params.userId;
  const programId = req.params.programId;

  var whereStatement = {};
  var whereStatementForProgram = {};
  if (userId) {
    whereStatement.id = { [Op.eq]: userId };
  }

  if (programId) {
    whereStatementForProgram.id = { [Op.eq]: programId };
  }

  User.findAll(({
    include: [
      {
        model: Program
        , as: 'ManagedPrograms'
        , through: { attributes: [] }
        // , required: false
        , where: whereStatementForProgram
      }
    ],
    where: whereStatement
    , attributes: ['id', 'name']
  }))
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};

//Find users with notifications.
exports.findAllWithNotifications = (req, res) => {
  const userId = req.params.userId;
  const isRead = req.params.isRead;

  var whereStatement = {};
  var whereStatementActiveOnly = {};
  if (userId)
    whereStatement.id = { [Op.eq]: userId };

  if (isRead)
    whereStatementActiveOnly.isRead = { [Op.eq]: isRead };

  console.log(whereStatementActiveOnly);

  User.findAll(({
    include: [
      {
        model: Notification
        , as: 'Notifications'
        , through: {
          as: 'NotificationDetails',
          attributes: ['id', 'isActive', 'isRead']
          , where: whereStatementActiveOnly
        }
        , attributes: ['id', 'title', 'description']
        , required: false
      }
    ],
    where: whereStatement
    , attributes: ['id', 'name']
  }))
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};

// Update a User by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  req.body.updatedBy = req.userId;
  User.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User with id=" + id
      });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  User.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User with id=" + id
      });
    });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
  User.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Users were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all users."
      });
    });
};

// find all pending User
exports.findAllPending = (req, res) => {
  User.findAll({ where: { status: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving ezpenses."
      });
    });
};


exports.getProgress = async (programId, studentId) => {

  return new Promise(function (resolve, reject) {

    let whereStatement = {};
    let whereStatementForProgram = {};

    if (studentId) {
      whereStatement.id = { [Op.eq]: studentId }
    }

    if (programId) {
      whereStatementForProgram.id = { [Op.eq]: programId }
    }

    User.findAll({
      include: [
        {
          model: Student,
          as: 'StudentDetails',
          attributes: ['totalAttendanceOfPrograms']
          , required: true
        },
        {
          model: Program
          , as: 'IncludedPrograms'
          , attributes: ['id', 'name', 'expectedDuration']
          , through: {
            as: 'ProgressOfProgram'
            , attributes: ['totalCompletion']
          }
          , where: whereStatementForProgram
        }
      ]
      , attributes: ['id', 'name']
      // , attributes: {
      //     include: ['id','name']
      //     // ,
      //     // exclude: ['password', 'createdBy', 'updatedBy', 'createdAt', 'updatedAt']
      // }
      , where: whereStatement
    })
      .then(data => {
        resolve(data);
      })
      .catch(err => {
        reject(new Error("Error retrieving Student with id=" + id, err));
      });
  });
};

