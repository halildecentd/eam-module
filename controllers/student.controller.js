const db = require("../models");
const Enums = require("../helpers/enums/enums");
const userController = require("../controllers/user.controller");


var bcrypt = require("bcryptjs");


const Program = db.programs;
const User = db.users;
const Student = db.students;
const Role = db.roles;
const Event = db.events;
const Notification = db.notifications;

const Op = db.Sequelize.Op;

// Create and Save a new Student
exports.create = (req, res) => {
    // Validate request
    if (!req.body.username || !req.body.email || !req.body.password || !req.body.name
        || !req.body.surname || !req.body.currentClass) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a Student
    const user = {
        name: req.body.name,
        middlename: req.body.middlename,
        surname: req.body.surname,
        mainRole: Enums.Roles.Student,
        username: req.body.username,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 8),
        isActive: req.body.isActive ? req.body.isActive : true,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id,
    };



    // Save Student in the database
    User.create(user)
        .then(user => {
            const studentInfo = {
                userId: user.id
                , currentClass: req.body.currentClass
                , totalAttendanceOfPrograms: req.body.totalAttendanceOfPrograms
            }
            Student.create(studentInfo)
                .then(student => {
                    Role.findAll({
                        where: {
                            name: {
                                [Op.eq]: Enums.Roles.Student
                            }
                        }
                    }).then(roles => {
                        user.setRoles(roles).then(() => {
                            res.send({ message: "Student was registered successfully!", data: student });
                        });
                    }).catch(err => {
                        res.status(500).send({ message: err.message });
                    });
                }
                ).catch(err => {
                    res.status(500).send({ message: err.message });
                });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

// Retrieve all Programs from the database.
exports.findAll = (req, res) => {

    var whereStatement = {};
    if (req.query.name) {
        whereStatement.name = { [Op.like]: '%' + req.query.name + '%' };
    }
    if (req.query.username) {
        whereStatement.username = { username: { [Op.like]: `%${req.query.username}%` } };
    }
    if (req.query.email) {
        whereStatement.email = { email: { [Op.like]: `%${req.query.email}%` } };
    }

    console.log(whereStatement);
    User.findAll({
        where: whereStatement,
        include: [
            {
                model: Student,
                as: 'StudentDetails',
                attributes: ['currentClass']
            },
            {
                model: Role,
                as: 'Roles',
                through: { attributes: [] },
                where: { name: { [Op.eq]: Enums.Roles.Student } }
            },
            {
                model: Notification,
                as: 'Notifications',
                through: { attributes: [] }
            },
            {
                model: Program,
                as: 'IncludedPrograms',
                through: { attributes: [] }
            }
        ],
        attributes: {
            include: [],
            exclude: []
        }
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedUserId = req.params.id;
    console.log("Related Student " + relatedUserId);
    var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

    var whereStatement = {};
    if (relatedUserId)
        whereStatement.id = relatedUserId;
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    User.findAll(({
        include: {
            model: Student
            , where: whereStatement
            , as: 'CreatorUser'
            , attributes: ['id', 'name', 'middlename', 'surname']

        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findPassedEvents = (req, res) => {

    const studentId = req.params.id;

    var whereStatement = {};

    whereStatement.eventDate = { [Op.lt]: Date.now() };
    if (req.query.name) {
        whereStatement.name = { [Op.like]: '%' + req.query.name + '%' };
    }
    if (req.query.username) {
        whereStatement.username = { username: { [Op.like]: `%${req.query.username}%` } };
    }
    if (req.query.email) {
        whereStatement.email = { email: { [Op.like]: `%${req.query.email}%` } };
    }

    Event.findAll(
        {
            where: whereStatement
            //, attributes: ['*']
            , include: [
                {
                    model: Program,
                    as: 'Program',
                    attributes: ['id', 'name'],
                    include: [{
                        model: User
                        , as: 'Participants'
                        , through: { attributes: [] }
                        , attributes: ['id', 'name']
                        , where: { id: { [Op.eq]: studentId } }
                    }]
                    , required: false
                }
            ]
            , order: [['eventDate', 'desc']]
        }
    )
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findUpcomingEvents = (req, res) => {

    const studentId = req.params.id;

    var whereStatement = {};

    whereStatement.eventDate = { [Op.gte]: Date.now() };
    if (req.query.name) {
        whereStatement.name = { [Op.like]: '%' + req.query.name + '%' };
    }
    if (req.query.username) {
        whereStatement.username = { username: { [Op.like]: `%${req.query.username}%` } };
    }
    if (req.query.email) {
        whereStatement.email = { email: { [Op.like]: `%${req.query.email}%` } };
    }
    Event.findAll(
        {
            where: whereStatement,
            include: [
                {
                    model: Program,
                    as: 'Program',
                    attributes: ['id', 'name'],
                    include: [{
                        model: User
                        , as: 'Participants'
                        , through: { attributes: [] }
                        , attributes: ['id', 'name']
                        , where: { id: { [Op.eq]: studentId } }
                    }]
                }
            ]
            //,attributes : ['Program.Participants.*']
            , order: [['eventDate', 'asc']]
        }
    )
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findRelatedEvents = (req, res) => {
    const source = req.query.source;
    const programId = req.params.id;

    var whereStatement = {};
    if (programId)
        whereStatement.id = { [Op.eq]: programId }
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    User.findOne(({
        include: [{
            model: Event
            , where: {}
            , as: 'Events'
            //, attributes: []
        }],
        attributes: ['id', 'name'],
        where: whereStatement
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single Student with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    User.findByPk(id, {
        include: [
            {
                model: Student,
                as: 'StudentDetails',
                attributes: ['currentClass']
            }]
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Student with id=" + id
            });
        });
};

// Find a single Student with an id
exports.getDetails = (req, res) => {
    const id = req.params.id;

    User.findOne({
        include: [
            {
                model: Student,
                as: 'StudentDetails',
                attributes: ['currentClass', 'totalAttendanceOfPrograms']
            }]
        , where: { id: { [Op.eq]: id } }
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Student with id=" + id, errorDetail: err.message
            });
        });
};

// Find a single Student with an id
exports.getProgress = (req, res) => {
    const studentId = req.params.studentId;
    const programId = req.params.programId;

    userController.getProgress(programId, studentId)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving progress", errorDetail: err.message
            });
        });
};

// Update a Student by the id in the request
exports.update = (req, res) => {
    const studentId = req.params.studentId;
    req.body.updatedBy = req.LoginUser.id;

    Student.findByPk(studentId)
        .then(student => {
            Student.update(req.body, {
                where: { id: studentId }
            })
                .then(num => {
                    if (num == 1) {
                        res.send({ message: "Student was updated successfully." });
                    } else {
                        res.send({
                            message: `Cannot update Student with id=${studentId}. Maybe Teacher was not found or req.body is empty!`
                        });
                    }
                })
                .catch(err => {
                    res.status(500).send({
                        message: "Error updating Student with id=" + studentId
                    });
                });

        });
};

// Delete a Student with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Student.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Student was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Student with id=${id}. Maybe Student was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Student with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
    Student.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Programs were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

