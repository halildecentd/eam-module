const db = require("../models");
const databaseOperation = require("../database/db.js");
const eventConroller = require("../controllers/event.controller");
const Program = db.programs;
const User = db.users;
const Event = db.events;
const Notification = db.notifications;
const ProgramManager = db.programManager;

const Op = db.Sequelize.Op;

// Create and Save a new Program
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name || !req.body.abbreviation || !req.body.type || !req.body.schoolLevelRecommendation ||
        !req.body.expectedDuration || !req.body.startDate || !req.body.endDate || !req.body.schoolYear
        || !req.body.programDirector || !req.body.ProgramManagers) {

        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a Program
    const program = {
        name: req.body.name,
        abbreviation: req.body.abbreviation,
        type: req.body.type,
        schoolLevelRecommendation: req.body.schoolLevelRecommendation,
        expectedDuration: req.body.expectedDuration,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        schoolYear: req.body.schoolYear,
        programDirector: req.body.programDirector,
        ProgramManagers: req.body.ProgramManagers,
        isActive: req.body.isActive ? req.body.isActive : false,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id
    };

    User.findAll({
        where: { id: { [Op.eq]: program.programDirector } }
    }).then(programDirector => {
        if (programDirector && programDirector.length > 0) {
            console.log(programDirector);

            User.findAll({
                where: { id: { [Op.or]: program.ProgramManagers } }
            }).then(programManagers => {
                if (programManagers && programManagers.length > 0) {

                    Program.create(program)
                        .then(program => {
                            program.setProgramManagers(programManagers).then(() => {
                                res.send({ message: `Program added successfully.`, data: program });
                            });
                        })
                        .catch(err => {
                            res.status(500).send({ message: err.message });
                        });
                }
                else {
                    res.status(400).send({
                        message: "Given Program Manager user(s) can't found.."
                    });
                }
            });
        }
        else {
            res.status(400).send({
                message: "Given Program Director user can't found.."
            });
        }
    });

    // Save Program in the database

};

// Retrieve all Programs from the database.
exports.findAll = (req, res) => {

    var whereStatement = {};
    if (req.query.name) {
        whereStatement.name = { [Op.like]: '%' + req.query.name + '%' };
    }
    if (req.query.username) {
        whereStatement.username = { username: { [Op.like]: `%${req.query.username}%` } };
    }
    if (req.query.email) {
        whereStatement.email = { email: { [Op.like]: `%${req.query.email}%` } };
    }

    console.log(whereStatement);
    Program.findAll({
        where: whereStatement,
        include: [
            {
                model: User,
                as: 'Participants',
                through: { attributes: [] }
            },
            {
                model: User,
                as: 'ProgramManagers',
                attributes: ['id', 'name', 'surname'],
                through: { attributes: [] }
            }
        ]
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedUserId = req.params.id;
    console.log("Related Program " + relatedUserId);
    var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

    var whereStatement = {};
    if (relatedUserId)
        whereStatement.id = relatedUserId;
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    Program.findAll(({
        include: {
            model: Program
            , where: whereStatement
            , as: 'CreatorUser'
            , attributes: ['id', 'name', 'middlename', 'surname']

        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findRelatedEvents = (req, res) => {
    const source = req.query.source;
    const programId = req.params.programId;

    var whereStatement = {};
    if (programId) {
        whereStatement.id = { [Op.eq]: programId }
    }
    // else {
    //     res.status(400).send({ messega: "Program id must be provided" });
    // }

    Program.findAll(({
        include: [{
            model: Event
            , where: {}
            , as: 'Events'
            //, attributes: []
            , required: false
        }],
        attributes: ['id', 'name']
        , where: whereStatement
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single Program with an id
exports.findOne = (req, res) => {
    const programId = req.params.programId;

    Program.findByPk(programId)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Program with id=" + programId
            });
        });
};

// Update a Program by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.userId;
    Program.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Program was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Program with id=${id}. Maybe Program was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Program with id=" + id
            });
        });
};

// Delete a Program with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Program.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Program was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Program with id=${id}. Maybe Program was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Program with id=" + id
            });
        });
};

// Delete all Programs from the database.
exports.deleteAll = (req, res) => {
    Program.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Programs were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

// Assign Participants For Program
exports.assignParticipantsToProgram = (req, res) => {
    if (!req.body.Participants || req.body.Participants.length <= 0) {
        res.status(400).send({
            message: "Participants for program must be provided 'Participants : [ userId1, userId2, ... ]' format.!"
        });
        return;
    }

    const programId = req.params.programId;
    var whereStatement = {};
    if (programId)
        whereStatement.id = programId;
    else {
        res.status(400).send({ messega: "Program id must be provided" });
    }

    Program.findByPk(programId)
        .then(program => {
            this.isUserApprovedForProgram(req.LoginUser.id, program.id)
                .then(isApproved => {
                    if (isApproved) {
                        let participantsForProgram = req.body.Participants;
                        User.findAll({
                            where: {
                                id: {
                                    [Op.or]: participantsForProgram
                                }
                            }
                        }).then(users => {
                            if (users && users.length > 0) {
                                program.setParticipants(users).then(() => {
                                    res.send({ message: `Users added for program : ${program.name}` })
                                });
                            }
                            else {
                                res.status(400).send({
                                    message: "Given Participant(s) can not be found in system.."
                                });
                            }
                        });
                    }
                    else {
                        res.status(403).send({
                            message: "User must have Admin, Program Director or Program Manager rights."
                        });
                    }
                });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while sending notifications for event."
            });
        });
};

// Assign Managers For Program
exports.assignManagersToProgram = (req, res) => {
    if (!req.body.ProgramManagers || !Array.isArray(req.body.ProgramManagers) || req.body.ProgramManagers.length <= 0) {
        res.status(400).send({
            message: "Managers for program must be provided 'ProgramManagers : [ userId1, userId2, ... ]' format.!"
        });
        return;
    }

    const programId = req.params.programId;
    var whereStatement = {};
    if (programId)
        whereStatement.id = programId;
    else {
        res.status(400).send({ messega: "Program id must be provided" });
    }

    Program.findByPk(programId)
        .then(program => {
            if (program) {
                let participantsForProgram = req.body.ProgramManagers;
                User.findAll({
                    where: {
                        id: {
                            [Op.or]: participantsForProgram
                        }
                    }
                }).then(users => {
                    if (users && users.length > 0) {
                        console.log(users);
                        program.setProgramManagers(users).then(() => {
                            res.send({ message: `Managers added for program : ${program.name}` })
                        });
                    }
                    else {
                        res.status(400).send({
                            message: "Given users can not be found in system.."
                        });
                    }
                });
            }
            else {
                res.status(403).send({
                    message: "Program with given id = " + programId + " not found"
                });
            }

        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while sending notifications for event."
            });
        });
};


exports.findAllWithParticipants = (req, res) => {
    const programId = req.params.programId;

    var whereStatementForProgram = {};
    var whereStatement = {};
    if (programId)
        whereStatementForProgram.id = { [Op.eq]: programId };

    Program.findAll(({
        include: [
            {
                model: User
                , as: 'Participants'
                , through: { attributes: [] }
                , required: false
            }
        ],
        where: whereStatementForProgram
        , attributes: ['id', 'name']
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};


// Send Notifications for Event to all user enrolled to program of event.
exports.sendNotificationsForUpcomingEvents = (req, res) => {

    const programId = req.params.id;
    console.log("Program Id :" + programId);

    if (!req.body.title || !req.body.description) {
        res.status(400).send({
            message: "Notification Content can not be empty!"
        });
        return;
    }

    var whereStatement = {};
    if (programId)
        whereStatement.id = programId;
    else {
        res.status(400).send({ messega: "Event id must be provided" });
    }


    eventConroller.findUpcomingEvents(req, res, programId)
        .then(data => {
            res.send(data);
        })

    // Program.findByPk(programId, ({
    //     include: [
    //         {
    //             model: User,
    //             as: 'Participants',
    //             through: { attributes: [] },
    //         }
    //     ]
    // }
    // ,{ where: whereStatement }
    // ))
    //     .then(program => {
    //         if (program) {
    //             console.log(program.Participants);

    //             this.isUserApprovedForProgram(req.LoginUser.id, program.id)
    //                 .then(isApproved => {
    //                     console.log("isApproved :" + isApproved);
    //                     if (isApproved) {

    //                         var whereStatement = {};
    //                         whereStatement.eventDate = { [Op.gte]: Date.now() };

    //                         // Event.findAll(
    //                         //     {
    //                         //         where: whereStatement,
    //                         //         include: [
    //                         //             {
    //                         //                 model: Program,
    //                         //                 as: 'Program',
    //                         //                 attributes: ['id', 'name'],
    //                         //                 include: [{
    //                         //                     model: User
    //                         //                     , as: 'Participants'
    //                         //                     , through: { attributes: [] }
    //                         //                     , attributes: ['id', 'name']
    //                         //                     , where: { id: { [Op.eq]: studentId } }
    //                         //                 }]
    //                         //             }
    //                         //         ]
    //                         //         , order: [['eventDate', 'asc']]
    //                         //     }
    //                         //         .then(data => {
    //                         //             res.send(data);
    //                         //         })
    //                         //         .catch(err => {
    //                         //             res.status(500).send({
    //                         //                 message:
    //                         //                     err.message || "Some error occurred while retrieving users."
    //                         //             });
    //                         //         });

    //                         let notificationTitle = req.body.title;
    //                         let notificationDescription = req.body.description;

    //                         Notification.create({ title: notificationTitle, description: notificationDescription, isActive: true, createdBy: req.LoginUser.id, updatedBy: req.LoginUser.id })
    //                             .then(notification => {
    //                                 notification.setNotifiedUsers(program.Participants);
    //                                 res.send({ notification });
    //                             });
    //                     }
    //                     else {
    //                         res.status(403).send({
    //                             message: "User must have Admin, Program Director or Program Manager rights."
    //                         });
    //                     }
    //                 })
    //         }
    //         else {
    //             res.status(404).send({
    //                 message: "Program not found with id : " + programId
    //             });
    //         }
    //     })
    //     .catch(err => {
    //         res.status(500).send({
    //             message:
    //                 err.message || "Some error occurred while sending notifications for event."
    //         });
    //     });
};


exports.isUserApprovedForProgram = async (userId, programId) => {

    let isAdminResult = true;
    isAdminResult = await getAdminUsersForProgram(userId, programId);
    return isAdminResult;
};

function getAdminUsersForProgram(userId, programId) {
    return new Promise(function (resolve, reject) {
        databaseOperation.query("CALL CheckIsAdminForProgram(?,?)", [userId, programId], function (err, result) {
            if (err) {
                reject(new Error('Ooops, something broke!', err));
            } else {
                var rows = JSON.parse(JSON.stringify(result[0]));
                var result = false;
                if (rows.length > 0) {
                    result = true;
                }
                resolve(result);
            }
        })
    })
        .catch(err => {
            throw new Error(err);
        });
}
