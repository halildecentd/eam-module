const db = require("../models");
const ProgramParticipant = db.programParticipant;
const User = db.users;
const Program = db.programs;
const Op = db.Sequelize.Op;

// Create and Save a new ProgramParticipant
exports.create = (req, res) => {
    // Validate request
    if (!req.body.userId || !req.body.programId) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a ProgramParticipant
    const programParticipant = {
        userId: req.body.userId,
        programId: req.body.programId,
        isActive: req.body.isActive ? req.body.isActive : true,
        totalCompletion: req.body.totalCompletion,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id
    };


    User.findByPk(programParticipant.userId).then(user => {
        if (user) {
            Program.findByPk(programParticipant.programId).then(program => {
                if (program) {
                    ProgramParticipant.create(programParticipant)
                        .then(programParticipant => {
                            res.send({ message: `User with id = ${user.id} added to program with id = ${program.id} successfully!`, data: programParticipant });
                        })
                        .catch(err => {
                            res.status(500).send({ message: err.message });
                        });
                }
                else {
                    res.status(500).send({ message: "Program is not found with id = " + programParticipant.programId });
                }
            });
        }
        else {
            res.status(500).send({ message: "User is not found with id = " + programParticipant.userId });
        }
    });
};

// Retrieve all Programs from the database.
exports.findAll = (req, res) => {

    var whereStatement = {};
    if (req.query.userId) {
        whereStatement.userId = { userId: { [Op.eq]: req.query.userId } };
    }

    console.log(whereStatement);
    ProgramParticipant.findAll({
        where: whereStatement
        // ,include :[
        //     {
        //         model: User
        //     }
        // ]

    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedUserId = req.params.id;
    var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

    var whereStatement = {};
    if (relatedUserId)
        whereStatement.id = relatedUserId;
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    ProgramParticipant.findAll(({
        include: {
            model: ProgramParticipant
            , where: whereStatement
            , as: 'CreatorUser'
            , attributes: ['id', 'name', 'middlename', 'surname']

        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single ProgramParticipant with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    ProgramParticipant.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving ProgramParticipant with id=" + id
            });
        });
};

// Update a ProgramParticipant by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.LoginUser.id;
    ProgramParticipant.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "ProgramParticipant was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update ProgramParticipant with id=${id}. Maybe ProgramParticipant was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating ProgramParticipant with id=" + id + ". Error details : " + err.message
            });
        });
};

// Delete a ProgramParticipant with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    ProgramParticipant.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "ProgramParticipant was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete ProgramParticipant with id=${id}. Maybe ProgramParticipant was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete ProgramParticipant with id=" + id
            });
        });
};

// Delete all ProgramParticipants from the database.
exports.deleteAll = (req, res) => {
    ProgramParticipant.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Programs were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};


