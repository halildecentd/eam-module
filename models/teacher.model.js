module.exports = (sequelize, Sequelize) => {
  const Teacher = sequelize.define("teacher", {
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    branch: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    }
  },
  {
      freezeTableName: true
  });

  Teacher.associate = function(models) {
    Teacher.belongsTo(models.User, {foreignKey: 'userId', as: 'user'})
  };
  return Teacher;
};
