module.exports = (sequelize, Sequelize) => {
    const UserRole = sequelize.define("user_role_rel", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        roleId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'role',
                key: 'id'
            }
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    },
        {
            freezeTableName: true
        });

    UserRole.associate = function (models) {
        UserRole.belongsTo(models.User, { foreignKey: 'userId', targetKey: 'id', as: 'User' });
        UserRole.belongsTo(models.Role, { foreignKey: 'roleId', targetKey: 'id', as: 'Role' });
    };

    return UserRole;
};
