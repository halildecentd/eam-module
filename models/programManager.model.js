module.exports = (sequelize, Sequelize) => {
    const ProgramManager = sequelize.define("program_manager_rel", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        programId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'program',
                key: 'id'
            }
        },
        userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    },
        {
            freezeTableName: true
        });

    ProgramManager.associate = function (models) {
        ProgramManager.belongsTo(models.User, { foreignKey: 'userId' });
        ProgramManager.belongsTo(models.Program, { foreignKey: 'programId' });
    };
    return ProgramManager;
};
