module.exports = (sequelize, Sequelize) => {
  const Student = sequelize.define("student", {
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    currentClass: {
      type: Sequelize.STRING(10),
      allowNull: false,
      defaultValue: true
    },
    totalAttendanceOfPrograms: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    }
  },

    {
      freezeTableName: true
    });

  Student.associate = function (models) {
    Student.belongsTo(models.User, { foreignKey: 'userId', as: 'User' })
  };
  return Student;
};
