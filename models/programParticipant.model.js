module.exports = (sequelize, Sequelize) => {
    const ProgramParticipant = sequelize.define("program_participant_rel", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        programId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'program',
                key: 'id'
            }
        },
        userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        },        
        totalCompletion: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    },
        {
            freezeTableName: true
        });

    ProgramParticipant.associate = function (models) {
        ProgramParticipant.belongsTo(models.User, { foreignKey: 'userId' })
        ProgramParticipant.belongsTo(models.Program, { foreignKey: 'programId' })
    };

    return ProgramParticipant;
};
