module.exports = (sequelize, Sequelize) => {
  const Staff = sequelize.define("staff", {
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    department: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    } 
  },
  {
      freezeTableName: true
  });

Staff.associate = function (models) {
  Staff.belongsTo(models.User, { foreignKey: 'userId', as: 'user' })
};
return Staff;
};
