const role = require('../helpers/enums/role.enum');

module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    },
    middlename: {
      type: Sequelize.STRING,
      allowNull: true
    },
    surname: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    },
    mainRole: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: role.Student
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true,
      unique: true
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true,
      unique: true
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    isActive: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    createdBy: {
      type: Sequelize.INTEGER,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    updatedBy: {
      type: Sequelize.INTEGER,
      references: {
        model: 'user',
        key: 'id'
      }
    }
  }
  ,{
    freezeTableName: true
  }
  );

  User.associate = function (models) {
    User.belongsToMany(models.Role, { as: 'Roles', through: models.UserRole, foreignKey: 'userId' });
    User.belongsToMany(models.Program, { as: 'IncludedPrograms', through: models.programParticipant, foreignKey: 'userId', otherKey: 'programId' });
    User.belongsToMany(models.Program, { as: 'ManagedPrograms', through: models.programManager, foreignKey: 'userId', otherKey: 'programId' });

    User.belongsToMany(models.Notification, { as: 'Notifications', through: models.UserNotification, foreignKey: 'userId', otherKey: 'notificationId' });


    User.hasMany(models.Event, { as: 'Events' });

    User.hasOne(models.Student, { as: 'StudentDetails', foreignKey: 'userId', targetKey: 'id' });
    User.hasOne(models.Student, { as: 'TeacherDetails', foreignKey: 'userId', targetKey: 'id' });
  };

  // User.associate = function(models) {
  // };

  return User;
};
