module.exports = (sequelize, Sequelize) => {
    const UserNotification = sequelize.define("user_notification_rel", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        notificationId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'notification',
                key: 'id'
            }
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        isRead: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
    },
        {
            freezeTableName: true
        });

    UserNotification.associate = function (models) {
        UserNotification.belongsTo(models.User, { foreignKey: 'userId', targetKey: 'id' })
        UserNotification.belongsTo(models.Notification, {foreignKey: 'notificationId', targetKey: 'id' })
    };

    return UserNotification;
};
