module.exports = (sequelize, Sequelize) => {
    const Program = sequelize.define("program", {        
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        abbreviation: {
            type: Sequelize.STRING(5),
            allowNull: false,
            defaultValue: true
        },
        type: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        schoolLevelRecommendation: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        expectedDuration: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        startDate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        endDate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        schoolYear: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        programDirector: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull : false,
            defaultValue : true
          },
        createdBy: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        updatedBy: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        }
    },
    {
        freezeTableName: true
    });

    Program.associate = function (models) {      
        Program.hasMany(models.Events, { as: 'Events', foreignKey: 'programId',sourceKey:'id'  })
        Program.belongsToMany(models.User, { as: 'Participants', through: models.programParticipant, foreignKey: 'programId', otherKey: 'userId' });
        Program.belongsToMany(models.User, { as: 'ProgramManagers', through: models.programManager, foreignKey: 'programId', otherKey: 'userId' });
    };
    return Program;
};
