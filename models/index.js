const dbConfig = require("../config/db.config.js");
const role = require("../helpers/enums/role.enum");

const Sequelize = require("sequelize");

var opts = {
  define: {
    //prevent sequelize from pluralizing table names
    freezeTableName: true
  }
}

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }, opts
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.roles = require("./role.model.js")(sequelize, Sequelize);
db.users = require("./user.model.js")(sequelize, Sequelize);
db.userroles = require("./userRole.model.js")(sequelize, Sequelize);

db.students = require("./student.model.js")(sequelize, Sequelize);
db.teachers = require("./teacher.model.js")(sequelize, Sequelize);
db.stafs = require("./staff.model.js")(sequelize, Sequelize);
db.programs = require("./program.model.js")(sequelize, Sequelize);
db.events = require("./event.model.js")(sequelize, Sequelize);
db.programManager = require("./programManager.model.js")(sequelize, Sequelize);
db.programParticipant = require("./programParticipant.model.js")(sequelize, Sequelize);

db.notifications = require("./notification.model.js")(sequelize, Sequelize);
db.userNotifications = require("./userNotification.model.js")(sequelize, Sequelize);


db.roles.belongsToMany(db.users, { as: 'UsersInRole', through: db.userroles, foreignKey: "roleId", targetKey: 'id', otherKey: "userId" });
db.users.belongsToMany(db.roles, { as: 'Roles', through: db.userroles, foreignKey: "userId", targetKey: 'id', otherKey: "roleId" });

db.programs.belongsToMany(db.users, { as: 'ProgramManagers', through: db.programManager, foreignKey: "programId", targetKey: 'id', otherKey: "userId" });
db.users.belongsToMany(db.programs, { as: 'ManagedPrograms', through: db.programManager, foreignKey: "userId", targetKey: 'id', otherKey: "programId" });

db.programs.belongsToMany(db.users, { as: 'Participants', through: db.programParticipant, foreignKey: "programId", targetKey: 'id', otherKey: "userId" });
db.users.belongsToMany(db.programs, { as: 'IncludedPrograms', through: db.programParticipant, foreignKey: "userId", targetKey: 'id', otherKey: "programId" });

db.notifications.belongsToMany(db.users, { as: 'NotifiedUsers', through: db.userNotifications, foreignKey: "notificationId", targetKey: 'id', otherKey: "userId" });
db.users.belongsToMany(db.notifications, { as: 'Notifications', through: db.userNotifications, foreignKey: "userId", targetKey: 'id', otherKey: "notificationId" });

db.programs.hasMany(db.events, { as: 'Events', foreignKey: 'programId', sourceKey: 'id' })
db.events.belongsTo(db.programs, {as: 'Program',  foreignKey: 'programId'  })


db.students.belongsTo(db.users, {foreignKey: 'userId', targetKey: 'id'});
db.users.hasOne(db.students, {as: 'StudentDetails', foreignKey: 'userId', targetKey: 'id'});

db.teachers.belongsTo(db.users, {foreignKey: 'userId', targetKey: 'id'});
db.users.hasOne(db.teachers, {as: 'TeacherDetails', foreignKey: 'userId', targetKey: 'id'});

db.notifications.belongsTo(db.events, {foreignKey: 'eventId', targetKey: 'id'});
db.events.hasMany(db.notifications, {as: 'Notifications', foreignKey: 'eventId', targetKey: 'id'});

module.exports = db;
