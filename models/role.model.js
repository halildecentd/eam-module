module.exports = (sequelize, Sequelize) => {
  const Role = sequelize.define("role", {   
    name: {
      type: Sequelize.STRING
    }
  },
  {
      freezeTableName: true
  }); 

  Role.associate = function(models) {
    // Role.belongsToMany(models.users, {through: 'user_role_rel', foreignKey: 'roleId', otherKey: 'userId', as: 'roles'});

    Role.belongsToMany(models.User, { as: 'UsersInRole', through: models.UserRole, foreignKey: 'roleId'});

  };
  return Role;
};
