const Enums = require('../helpers/enums/enums');


module.exports = (sequelize, Sequelize) => {
    const Notification = sequelize.define("notification", {
        eventId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'event',
                key: 'id'
            }
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        notificationType: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: Enums.NotificationTypes.NewEvent
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        createdBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        updatedBy: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'user',
                key: 'id'
            }
        }
    },
        {
            freezeTableName: true
        });

    Notification.associate = function (models) {
        Notification.belongsToMany(models.User, { as: 'NotifiedUsers', through: models.UserNotification, foreignKey: 'notificationId' });
        Notification.belongsTo(models.Event, { as: 'Event', foreignKey: 'eventId', targetKey: 'id' });
    };

    return Notification;
};
