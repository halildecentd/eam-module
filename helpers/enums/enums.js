roleEnum = {
    Admin: 'Admin',
    User: 'User',
    Student: 'Student',
    Staff: 'Staff',
    Teacher: 'Teacher',
    ProgramManager: 'ProgramManager',
    ProgramDirector: 'ProgramDirector'
}

notificationTypeEnum = {
    NewEvent: 'NewEvent',
    UpcomingEvents: 'UpcomingEvents'
}

const Enums = {
    Roles: roleEnum,
    NotificationTypes: notificationTypeEnum
}

module.exports = Enums;

