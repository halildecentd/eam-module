
module.exports = {
    Admin: 'Admin',
    User: 'User',
    Student : 'Student',
    Staff : 'Staff',
    Teacher : 'Teacher',
    ProgramManager : 'ProgramManager',
    ProgramDirector : 'ProgramDirector'
}