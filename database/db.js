const mysql = require("mysql2");
const dbConfig = require("../config/db.config.js");

// var connection = mysql.createConnection({
//     host: dbConfig.HOST,
//     user: dbConfig.USER,
//     password: dbConfig.PASSWORD,
//     database: dbConfig.DB
// });

var pool = mysql.createPool({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});

//const promisePool = pool.promise();

// const databaseOperation = {
//     connection: connection,
//     pool: promisePool
//   };

module.exports = pool;