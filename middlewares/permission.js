const programController = require("../controllers/program.controller.js");

module.exports = function permit(allowedRolesArray, checkForProgramAdmins) {

  const isAllowed = roles => roles.some(item => allowedRolesArray.includes(item)); // allowed.indexOf(role) > -1;   
  // return a middleware
  return (req, res, next) => {
    console.log("************************");
    console.log(req.LoginUser);

    //args.forEach(arg => console.log(arg))
    if (req.LoginUser && isAllowed(req.LoginUser.Roles)) {
      next(); // role is allowed, so continue on the next middleware
    }
    else if (checkForProgramAdmins) {

      console.log("CHECK FOR PROGRAM ADMINS: " + checkForProgramAdmins);

      const programId = req.params.programId;
      console.log("ProgramId: " + programId);

      programController.isUserApprovedForProgram(req.LoginUser.id, programId)
        .then(result => {
          if (result) {
            console.log("Hello :::: " + result);
            next();
          }
          else {
            res.status(403).json({ message: "Forbidden" }); // user is forbidden
          }
        });
    }
    else {
      res.status(403).json({ message: "Forbidden" }); // user is forbidden
    }
  }
}